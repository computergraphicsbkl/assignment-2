﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp3
{
    public partial class Form1 : Form
    {

        const string PATH = "../../rocket.bmp";
        Bitmap base_image;


        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            pictureBox1.Image = Image.FromFile(PATH);
            pictureBox1.SizeMode = PictureBoxSizeMode.Normal;
            base_image = new Bitmap(pictureBox1.Image);
            chart1.Legends.Clear();
            chart1.ChartAreas[0].AxisX.MajorGrid.Enabled = false;
            chart1.ChartAreas[0].AxisX.MinorGrid.Enabled = false;
            chart1.ChartAreas[0].AxisY.MajorGrid.Enabled = false;
            chart1.ChartAreas[0].AxisY.MinorGrid.Enabled = false;
            //chart1.ChartAreas[0].AxisY.LineColor = chart1.BackColor;
            chart1.ChartAreas[0].AxisY.LabelStyle.Enabled = false;

            Draw_Graph(base_image);
        }


        private void Change_Picture(char channel)
        {
            if (pictureBox1.Image != null)
            {
                Bitmap output = new Bitmap(base_image.Width, base_image.Height);
                switch (channel)
                {
                    case 'R':
                        {
                            Red_Channel(output);
                            break;
                        }
                    case 'G':
                        {
                            Green_Channel(output);
                            break;
                        }
                    case 'B':
                        {
                            Blue_Channel(output);
                            break;
                        }
                    case '0':
                        {
                            Gray_Shades(output);
                            break;
                        }
                    default:
                        throw new Exception("Wrong color");
                }             
                pictureBox1.Image = output;
                Draw_Graph(output);
            }
        }


        private void Red_Channel(Bitmap output)
        {
            for (int j = 0; j < base_image.Height; j++)
            {
                for (int i = 0; i < base_image.Width; i++)
                {
                    UInt32 pixel = (UInt32)(base_image.GetPixel(i, j).ToArgb());

                    UInt32 R = (pixel & 0x00FF0000) >> 16;

                    UInt32 newPixel = 0xFF000000 | (R << 16);

                    output.SetPixel(i, j, Color.FromArgb((int)newPixel));
                }
            }
        }


        private void Green_Channel(Bitmap output)
        {
            for (int j = 0; j < base_image.Height; j++)
            {
                for (int i = 0; i < base_image.Width; i++)
                {
                    UInt32 pixel = (UInt32)(base_image.GetPixel(i, j).ToArgb());

                    UInt32 G = (pixel & 0x0000FF00) >> 8;

                    UInt32 newPixel = 0xFF000000 | (G << 8);

                    output.SetPixel(i, j, Color.FromArgb((int)newPixel));
                }
            }
        }


        private void Blue_Channel(Bitmap output)
        {
            for (int j = 0; j < base_image.Height; j++)
            {
                for (int i = 0; i < base_image.Width; i++)
                {
                    UInt32 pixel = (UInt32)(base_image.GetPixel(i, j).ToArgb());

                    UInt32 B = pixel & 0x000000FF;

                    UInt32 newPixel = 0xFF000000 | B;

                    output.SetPixel(i, j, Color.FromArgb((int)newPixel));
                }
            }
        }

        private void Gray_Shades(Bitmap output)
        {
            for (int j = 0; j < base_image.Height; j++)
            {
                for (int i = 0; i < base_image.Width; i++)
                {
                    UInt32 pixel = (UInt32)(base_image.GetPixel(i, j).ToArgb());

                    float R = (float)((pixel & 0x00FF0000) >> 16);
                    float G = (float)((pixel & 0x0000FF00) >> 8);
                    float B = (float)(pixel & 0x000000FF);

                    R = G = B = (R + G + B) / 3.0f;

                    UInt32 newPixel = 0xFF000000 | ((UInt32)R << 16) | ((UInt32)G << 8) | (UInt32)B;

                    output.SetPixel(i, j, Color.FromArgb((int)newPixel));
                }
            }
        }


        private void redToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Change_Picture('R');
        }

        private void greenToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Change_Picture('G');
        }

        private void blueToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Change_Picture('B');
        }

        private void grayShadesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Change_Picture('0');
        }

        private void baseImageToolStripMenuItem_Click(object sender, EventArgs e)
        {
            pictureBox1.Image = base_image;
            Draw_Graph(base_image);
        }


        private void Draw_Graph(Bitmap bm)
        {
            chart1.Series[0].Points.Clear();
            long sum_red = 0;
            long sum_green = 0;
            long sum_blue = 0;

            for (int j = 0; j < bm.Height; j++)
            {
                for (int i = 0; i < bm.Width; i++)                {
                    UInt32 pixel = (UInt32)(bm.GetPixel(i, j).ToArgb());

                    UInt32 R = (pixel & 0x00FF0000) >> 16;
                    UInt32 G = (pixel & 0x0000FF00) >> 8;
                    UInt32 B = pixel & 0x000000FF;

                    sum_red += R;
                    sum_green += G;
                    sum_blue += B;
                }

                int k = 0;

                System.Diagnostics.Debug.WriteLine($"GGGGGGG\n");
            }

            //chart1.Series[0].

            chart1.Series[0].Points.AddXY("RED", sum_red);
            chart1.Series[0].Points.AddXY("GREEN", sum_green);
            chart1.Series[0].Points.AddXY("BLUE", sum_blue);

            chart1.Series[0].Points[0].Color = Color.Red;
            chart1.Series[0].Points[1].Color = Color.Green;
            chart1.Series[0].Points[2].Color = Color.Blue;
        }
    }
}
