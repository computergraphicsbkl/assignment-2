from tkinter import *
from tkinter import filedialog
from PIL import Image, ImageTk
import os.path
from math import floor

def dialog_open():
    return filedialog.askopenfilename(initialdir="~", filetypes=[("JPG, JPEG", "*.jpg *.jpeg"), ("All files", "*.*")])

def dialog_save():
    return filedialog.asksaveasfilename(initialdir="~", filetypes=[("JPG, JPEG", "*.jpg *.jpeg"), ("All files", "*.*")])

def file_open():
    global image
    filename = dialog_open()
    if filename == ():
        return
    if not os.path.isfile(filename):
        return
    root.title(filename + " - HSV")
    image = Image.open(filename)
    show_image()

def file_save():
    global transformed_image
    filename = dialog_save()
    transformed_image.save(filename)

def show_image():
    global imagetk, transformed_image
    if image is None:
        return
    transformed_image = transform(image)
    imagetk = ImageTk.PhotoImage(transformed_image)
    canvas.delete("all")
    canvas.create_image(0, 0, image=imagetk, anchor=NW)

def transform(image):
    print(delta_hue.get(), delta_saturation.get(), delta_value.get())
    transformed_image = image.copy()
    pixels_rgb = transformed_image.load()
    pixels_hsv = []
    for x in range(transformed_image.width):
        row = []
        for y in range(transformed_image.height):
            row.append(rgb_to_hsv(pixels_rgb[x,y]))
        pixels_hsv.append(row)
    for x in range(len(pixels_hsv)):
        for y in range(len(pixels_hsv[x])):
            h = pixels_hsv[x][y][0]
            s = pixels_hsv[x][y][1]
            v = pixels_hsv[x][y][2]
            h = (h + delta_hue.get()) % 360
            s = min(max((s + delta_saturation.get()), 0), 1)
            v = min(max((v + delta_value.get()), 0), 1)
            pixels_hsv[x][y] = (h, s, v)
    for x in range(len(pixels_hsv)):
        for y in range(len(pixels_hsv[x])):
            pixels_rgb[x,y] = hsv_to_rgb(pixels_hsv[x][y])
    return transformed_image

def rgb_to_hsv(pixel_rgb):
    (r, g, b) = pixel_rgb
    r /= 255.
    g /= 255.
    b /= 255.
    max_ = max(r, g, b)
    min_ = min(r, g, b)
    h = 0 if max_ == min_ \
        else (60 * (g-b)/(max_-min_) + 0) if max_ == r and g >= b \
        else (60 * (g-b)/(max_-min_) + 360) if max_ == r and g < b \
        else (60 * (b-r)/(max_-min_) + 120) if max_ == g \
        else (60 * (r-g)/(max_-min_) + 240)
    s = 0 if max_ == 0 \
        else (1 - min_/max_)
    v = max_
    h = int(h)
    return (h, s, v)

def hsv_to_rgb(pixel_hsv):
    (h, s, v) = pixel_hsv
    s *= 100.
    v *= 100.
    h_i = int(floor(h / 60.)) % 6
    v_min = (100-s)*v / 100.
    a = (v - v_min) * (h % 60) / 60.
    v_inc = v_min + a
    v_dec = v - a
    (r, g, b) = (v, v_inc, v_min) if h_i == 0 \
        else (v_dec, v, v_min) if h_i == 1 \
        else (v_min, v, v_inc) if h_i == 2 \
        else (v_min, v_dec, v) if h_i == 3 \
        else (v_inc, v_min, v) if h_i == 4 \
        else (v, v_min, v_dec)
    r = int(r * 255/100.)
    g = int(g * 255/100.)
    b = int(b * 255/100.)
    return (r, g, b)

root = Tk()
root.geometry("800x600")
root.title("HSV")
root.update()

delta_hue = IntVar()
delta_saturation = DoubleVar()
delta_value = DoubleVar()
image = None

button_open = Button(root, text="Open", command=file_open)
button_save = Button(root, text="Save", command=file_save)
label_hue = Label(root, text="Hue")
scale_hue = Scale(root, from_=-180, to=180, orient="horizontal", variable=delta_hue, length=200)
label_saturation = Label(root, text="Saturation")
scale_saturation = Scale(root, from_=-1, to=1, resolution=0.1, orient="horizontal", variable=delta_saturation)
label_value = Label(root, text="Value")
scale_value = Scale(root, from_=-1, to=1, resolution=0.1, orient="horizontal", variable=delta_value)
button_show = Button(root, text="Show", command=show_image)
canvas = Canvas(root, width=root.winfo_width(), height=root.winfo_height()-30)

button_open.grid(row=0, column=0, padx=(2, 0))
button_save.grid(row=0, column=1, padx=(2, 0))
label_hue.grid(row=0, column=2, padx=(2, 0))
scale_hue.grid(row=0, column=3, padx=(2, 0))
label_saturation.grid(row=0, column=4, padx=(2, 0))
scale_saturation.grid(row=0, column=5, padx=(2, 0))
label_value.grid(row=0, column=6, padx=(2, 0))
scale_value.grid(row=0, column=7, padx=(2, 0))
button_show.grid(row=0, column=8, padx=(2, 0))
canvas.grid(row=1, column=0, columnspan=9)

root.mainloop()