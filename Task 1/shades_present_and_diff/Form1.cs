﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Security;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace shades_present_and_diff
{
    public partial class Form1 : Form
    {
        private const string DEFAULT_FILE_PATH = "../../sample2.png";
        private const string SAMPLE2_FILE_PATH = "../../sample1.jpeg";
        private const string SAMPLE3_FILE_PATH = "../../sample3.jpg";

        Bitmap image_to_process;

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            ProcessFile(DEFAULT_FILE_PATH);
        }

        private void Label1_Click(object sender, EventArgs e)
        {

        }

        private void ExitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void ProcessFile(string filename)
        {
            // Load image
            pictureBox1.Image = Image.FromFile(filename);
            pictureBox1.SizeMode = PictureBoxSizeMode.StretchImage;

            //Store image bitmap 
            image_to_process = new Bitmap(pictureBox1.Image);

            //Convert stored image into shades with NTSC formula
            Bitmap ntsc_rgb_processed_image = ProcessImageWithNTSCSRGBFormula(new Bitmap(pictureBox1.Image));

            //Load converted image into UI
            pictureBox2.SizeMode = PictureBoxSizeMode.StretchImage;
            pictureBox2.Image = ntsc_rgb_processed_image;

            chart1.Series[0].Points.Clear();
            chart1.ChartAreas["ChartArea1"].AxisX.Interval = 5;
            foreach (var keyValuePair in ShadesCounter(ntsc_rgb_processed_image))
            {
                chart1.Series[0].Points.AddXY(keyValuePair.Key, keyValuePair.Value);
            }



            //Convert stored image into shades with HDTV formula
            Bitmap s_rgb_processed_image = ProcessImageWithSRGBFormula(new Bitmap(pictureBox1.Image));

            //Load converted image into UI
            pictureBox3.SizeMode = PictureBoxSizeMode.StretchImage;
            pictureBox3.Image = s_rgb_processed_image;

            chart2.Series[0].Points.Clear();
            chart2.ChartAreas["ChartArea1"].AxisX.Interval = 5;
            foreach (var keyValuePair in ShadesCounter(s_rgb_processed_image))
            {
                chart2.Series[0].Points.AddXY(keyValuePair.Key, keyValuePair.Value);
            }


            //Calculate diff
            Bitmap diff_processed = ProcessImageWithDiffFormula(new Bitmap(pictureBox3.Image), new Bitmap(pictureBox2.Image));

            //Load calculated diff into UI
            pictureBox4.SizeMode = PictureBoxSizeMode.StretchImage;
            pictureBox4.Image = diff_processed;
        }

        private Bitmap ProcessImageWithNTSCSRGBFormula(Bitmap thing)
        {
            Bitmap output = thing;

            for (int j = 0; j < output.Height; j++)
                for (int i = 0; i < output.Width; i++)
                {
                    uint pixel1 = (uint)output.GetPixel(i, j).ToArgb();

                    //extract components
                    float R1 = (float)((pixel1 & 0x00FF0000) >> 16);
                    float G1 = (float)((pixel1 & 0x0000FF00) >> 8);
                    float B1 = (float)(pixel1 & 0x000000FF);

                    //calculate new pixel value
                    
                    float Y = Convert.ToSingle(0.299 * R1 + 0.587 * G1 + 0.114 * B1);
                    uint newPixel = 0xFF000000 | ((uint)Y << 16) | ((uint)Y << 8) | (uint)Y;
                    output.SetPixel(i, j, Color.FromArgb((int)newPixel));

                }

            return output;
        }

        private Bitmap ProcessImageWithSRGBFormula(Bitmap thing)
        {
            Bitmap output = thing;

            for (int j = 0; j < output.Height; j++)
                for (int i = 0; i < output.Width; i++)
                {
                    uint pixel1 = (uint)image_to_process.GetPixel(i, j).ToArgb();

                    //extract components
                    float R1 = (float)((pixel1 & 0x00FF0000) >> 16);
                    float G1 = (float)((pixel1 & 0x0000FF00) >> 8);
                    float B1 = (float)(pixel1 & 0x000000FF);

                  
                    float Y = Convert.ToSingle(0.2126 * R1 + 0.7152 * G1 + 0.0722 * B1);
                    uint newPixel = 0xFF000000 | ((uint)Y << 16) | ((uint)Y << 8) | (uint)Y;
                    output.SetPixel(i, j, Color.FromArgb((int)newPixel));
 
                }

            return output;
        }

        private Bitmap ProcessImageWithDiffFormula(Bitmap first_bitmap, Bitmap second_bitmap = null)
        {
            Bitmap output = first_bitmap;
            Dictionary<int, int> dict_output = new Dictionary<int, int>();


            float min = 255;
            for (int j = 0; j < first_bitmap.Height; j++)
                for (int i = 0; i < first_bitmap.Width; i++)
                {
                    uint pixel1 = (uint)first_bitmap.GetPixel(i, j).ToArgb();

                    //extract components
                    float R1 = (float)((pixel1 & 0x00FF0000) >> 16);

                    //calculate new pixel value

                    uint pixel2 = (uint)second_bitmap.GetPixel(i, j).ToArgb();

                    //extract components
                    float R2 = (float)((pixel2 & 0x00FF0000) >> 16);

                    float diff = R1 - R2;

                    if (diff < min)
                        min = diff;

                    uint newPixel = 0xFF000000 | ((uint)diff << 16) | ((uint)diff << 8) | (uint)diff;
                    output.SetPixel(i, j, Color.FromArgb((int)newPixel));
                }

            if (min < 0)
            {
                for (int j = 0; j < first_bitmap.Height; j++)
                    for (int i = 0; i < first_bitmap.Width; i++)
                    {
                        uint pixel1 = (uint)first_bitmap.GetPixel(i, j).ToArgb();

                        //extract components
                        //float R1 = (float)((pixel1 & 0x00FF0000) >> 16) + min;
                        float R1 = (float)((pixel1 & 0x00FF0000) >> 16) - min;

                        //calculate new pixel value

                        uint newPixel = 0xFF000000 | ((uint)R1 << 16) | ((uint)R1 << 8) | (uint)R1;
                        output.SetPixel(i, j, Color.FromArgb((int)newPixel));
                    }
            }

          
                
            return output;
        }

        private double ColorsDiff(float R1, float G1, float B1, float R2, float G2, float B2)
        {
            return Math.Sqrt(Math.Pow(R2-R1, 2)+Math.Pow(G2-G1, 2)+Math.Pow(B2-B1, 2));
        }

        private void LoadImageToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    ProcessFile(openFileDialog1.FileName);

                }
                catch (Exception)
                {
                    /* MessageBox.Show($"Security error.\n\nError message: {ex.Message}\n\n" +
                     $"Details:\n\n{ex.StackTrace}");*/
                }
            }

        }

        private void LoadSample2ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ProcessFile(SAMPLE2_FILE_PATH);

        }

        private void LoadSample1ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ProcessFile(DEFAULT_FILE_PATH);
        }

        private Dictionary<int, int> ShadesCounter(Bitmap thing)
        {
            Dictionary<int, int> output = new Dictionary<int, int>();

            for (int j = 0; j < thing.Height; j++)
                for (int i = 0; i < thing.Width; i++)
                {
                    uint pixel1 = (uint)image_to_process.GetPixel(i, j).ToArgb();

                    
                    //extract components
                    float R1 = (float)((pixel1 & 0x00FF0000) >> 16);
                    float G1 = (float)((pixel1 & 0x0000FF00) >> 8);
                    float B1 = (float)(pixel1 & 0x000000FF);

                    float Y = Convert.ToSingle(0.299 * R1 + 0.587 * G1 + 0.114 * B1);
                    
                    output.TryGetValue((int)Y+1, out var currentCount);
                    output[(int)Y+1] = currentCount + 1;
                }

            return output;
        }

        private void LoadSmple3ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ProcessFile(SAMPLE3_FILE_PATH);

        }

        private void PictureBox1_Click(object sender, EventArgs e)
        {

        }
    }
}
